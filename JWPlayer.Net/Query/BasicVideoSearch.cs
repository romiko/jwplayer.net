﻿using System;
using Newtonsoft.Json;

namespace JWPlayer.Net.Query
{
    public class BasicVideoSearch
    {
        public BasicVideoSearch()
        {
            ResultLimit = 100;
            ResultOffset = 0;
        }

        /// <summary>
        /// Only videos that contain search string in at least one of the default search fields will be listed. Default search fields are: author, description, tags, title and video_key.
        /// </summary>
        [JsonProperty("search")]
        public string Search { get; set; }

        /// <summary>
        /// Video creation date starting from which videos list should be returned. Date must be an UTC date in Unix timestamp format.
        /// Default is -100 days from Today UTC.
        /// </summary>
        [JsonProperty("start_date")]
        public DateTimeOffset StartDate { get; set; }

        /// <summary>
        /// Specifies maximum number of videos to return. Default is 100. Maximum result limit is 1000.
        /// https://developer.jwplayer.com/jw-platform/reference/v1/methods/videos/list.html
        /// </summary>
        [JsonProperty("result_limit")]
        public int ResultLimit { get; set; }

        /// <summary>
        /// Specifies how many videos should be skipped at the beginning of the result set. Default is 0.
        /// https://developer.jwplayer.com/jw-platform/reference/v1/methods/videos/list.html
        /// </summary>
        [JsonProperty("result_offset")]
        public int ResultOffset { get; set; }
        
    }
}