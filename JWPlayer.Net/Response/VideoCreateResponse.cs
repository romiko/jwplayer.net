﻿namespace JWPlayer.Net.Response
{
    public class VideoCreateResponse : BaseResponse
    {
        public Media Media { get; set; }
        public Link Link { get; set; }
        public Video Video { get; set; }
    }
}