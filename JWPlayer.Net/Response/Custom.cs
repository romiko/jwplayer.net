namespace JWPlayer.Net.Response
{
    public class Custom
    {
        public string ThumbnailUrl { get; set; }
        public string LegacyId { get; set; }
    }
}