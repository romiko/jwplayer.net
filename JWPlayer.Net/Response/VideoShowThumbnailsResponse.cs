﻿namespace JWPlayer.Net.Response
{
    public class VideoShowThumbnailsResponse : BaseResponse
    {
        public Thumbnail Thumbnail { get; set; }
}
}