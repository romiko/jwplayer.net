namespace JWPlayer.Net.Response
{
    public class Link
    {
        public string Path { get; set; }
        public Query Query { get; set; }
        public string Protocol { get; set; }
        public string Address { get; set; }
    }
}