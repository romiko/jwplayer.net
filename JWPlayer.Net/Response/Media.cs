namespace JWPlayer.Net.Response
{
    public class Media
    {
        public string Type { get; set; }
        public string Key { get; set; }
    }
}