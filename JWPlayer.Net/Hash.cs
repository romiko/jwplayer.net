﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using RestSharp;
using RestSharp.Extensions.MonoHttp;

namespace JWPlayer.Net
{
    public class Hash
    {
        public static  string SignQueryString(List<Parameter> requestParameters, string apiSecret)
        {
            var sbs = PrepareQueryStrings(requestParameters);
            sbs += apiSecret;
            var bytes = Encoding.UTF8.GetBytes(sbs);
            using (var sha1 = SHA1.Create())
            {
                var hash = sha1.ComputeHash(bytes);
                var sb = new StringBuilder();
                foreach (var b in hash)
                    sb.Append(b.ToString("X2"));

                return sb.ToString();
            }
        }

        public static string PrepareQueryStrings(List<Parameter> requestParameters)
        {
            var sbs = string.Empty;
            foreach (var param in requestParameters.OrderBy(r => r.Name))
            {
                if (sbs != string.Empty)
                    sbs += "&";

                sbs +=
                    ($"{HttpUtility.UrlEncode(param.Name)}={HttpUtility.UrlEncode(param.Value.ToString(), Encoding.UTF8).Replace("+", "%20")}");
            }

            sbs = Regex.Replace(sbs, @"%[a-f\d]{2}", m => m.Value.ToUpper());
            return sbs;
        }
    }
}
