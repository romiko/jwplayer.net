﻿using System.Collections.Generic;
using JWPlayer.Net.Query;
using JWPlayer.Net.Response;

namespace JWPlayer.Net
{
    public interface IJw
    {
        /// <summary>
        /// https://developer.jwplayer.com/jw-platform/reference/v1/methods/videos/list.html
        /// </summary>
        /// <param name="searchQuery"></param>
        /// <returns></returns>
        VideoListResponse ListVideos(BasicVideoSearch searchQuery);

        VideoListResponse ListVideos();

        /// <summary>
        /// https://developer.jwplayer.com/jw-platform/reference/v1/methods/videos/create.html
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        VideoCreateResponse CreateVideo(Dictionary<string,string> parameters);

        /// <summary>
        /// https://developer.jwplayer.com/jw-platform/reference/v1/methods/videos/update.html
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        VideoCreateResponse UpdateVideo(Dictionary<string, string> parameters);


        /// <summary>
        /// https://developer.jwplayer.com/jw-platform/reference/v1/methods/videos/show.html
        /// </summary>
        /// <param name="videoKey"></param>
        /// <returns></returns>
        VideoShowResponse ShowVideo(string videoKey);

        /// <summary>
        /// https://developer.jwplayer.com/jw-platform/reference/v1/methods/videos/thumbnails/show.html
        /// </summary>
        /// <param name="videoKey"></param>
        /// <returns></returns>
        VideoShowThumbnailsResponse ShowVideoThumbails(string videoKey);

        /// <summary>
        /// Use either Position or ThumbnailIndex - Not Both
        /// https://developer.jwplayer.com/jw-platform/reference/v1/methods/videos/thumbnails/update.html
        /// </summary>
        /// <param name="videoKey"></param>
        /// <param name="position"></param>
        /// <param name="thumbnailIndex"></param>
        /// <returns></returns>
        VideoUpdateThumbnailsResponse UpdateVideoThumbails(string videoKey, int? size, string position, string thumbnailIndex);
   }
}