# JWPlayer .NET API Library #

.NET Developers can use this library to batch import videos and query videos on JWPlayer Hosted Platform.

Ensure the following is in your app.config
```
#!xml
    <add key="ApiKey" value="myKey"/>
    <add key="ApiSecret" value="mySecret"/>
```

## List Videos with Filter ##

```
#!c#

var jw = new Jw(ApiKey, ApiSecret);
var basicVideoSearch = new BasicVideoSearch {Search = "Foo", StartDate = DateTime.UtcNow.AddDays(-100)};
var result = jw.ListVideos(basicVideoSearch);
var count = result.Videos.Count;
```
## List Videos with no search term and a date filter ##

```
#!c#

var jw = new Jw(ApiKey, ApiSecret);
var basicVideoSearch = new BasicVideoSearch {StartDate = DateTime.UtcNow.AddDays(-100)};
var result = jw.ListVideos(basicVideoSearch);
var count = result.Videos.Count;
```

## List All Videos ##

```
#!c#
var jw = new Jw(ApiKey, ApiSecret);
var result = jw.ListVideos();
var count = result.Videos.Count;
```

## Create Video ##

```
#!c#

var jw = new Jw(ApiKey, ApiSecret);
var parameters = new Dictionary<string, string>
{
    {"sourceurl", "http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_1mb.mp4"},
    {"sourceformat", "mp4"},
    {"sourcetype", "url"},
    {"title", "Test"},
    {"description", "Test Video"},
    {"tags", "foo, bar"},
    {"custom.LegacyId", Guid.NewGuid().ToString()}
};
var result = jw.CreateVideo(parameters);

```
# Update Video ##

```
#!c#

var jw = new Jw(ApiKey, ApiSecret);
var parameters = new Dictionary<string, string>
{
    {"video_key", "QxbbRMMP"},
    {"title", "Test Updated"},
    {"tags", "foo, bar, updated"},
};
var result = jw.UpdateVideo(parameters);

```

# Show Video ##

```
#!c#

var jw = new Jw(ApiKey, ApiSecret);
var video = ConfigurationManager.AppSettings["TestVideoJWPlayerKey"];
var result = jw.ShowVideo(video);

```

# Reponse Models ##
All data is automatically deserialized into .NET Objects e.g. Video, Media, Custom etc. However, there may be times where you want the raw JSON. e.g.
```
#!c#

var jw = new Jw(ApiKey, ApiSecret);
var basicVideoSearch = new BasicVideoSearch { Search = "Foo", StartDate = DateTime.UtcNow.AddDays(-100) };
var result = jw.ListVideos(basicVideoSearch);
var jsonResult = result.Json;
```

# Batch Migrations ##
You can perform large scale migrations of videos. Just pass a parameter for the source of your videos e.g. sourceurl, sourceformat, sourcetype when calling CreateVideo.

There is a Rate Limit of 60 calls per minute. So ensure you throttle your calls to one call per second e.g.
```
#!c#

for(var i = 0; i < lines.count; i++)
{
   jw.CreateVideo(parameters);
   Thread.Sleep(TimeSpan.FromSeconds(1));
}
```
You can see the current Rate Limit applied in the result.
```
#!c#

var jw = new Jw(ApiKey, ApiSecret);
var video = ConfigurationManager.AppSettings["TestVideoJWPlayerKey"];
var result = jw.ShowVideo(video);
var ratelimit = result.RateLimit;
```

Copyright 2017 Romiko Derbynew

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.